readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly MODEL="X86"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

for file in $(ls $LITMUS_DIR); do
  herd_clean X86 "$file"
  herd_clean jam "$file"
done