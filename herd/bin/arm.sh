readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly MODEL="arm"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

# generating litmus test
$SCRIPT_DIR/tolitmus.sh $LITMUS_DIR/hotspot-sc.test > $LITMUS_DIR/hotspot-sc.litmus
herd_simple $MODEL "hotspot-sc.litmus"
