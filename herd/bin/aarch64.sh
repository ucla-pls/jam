readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
readonly MODEL="aarch64"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

for file in $(ls $LITMUS_DIR | grep -v "misc"); do
  herd_clean aarch64 "$file"
  herd_clean jam "$file"
done
