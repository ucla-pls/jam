readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
set -e

MODEL="X86"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

herd_clean X86 "SB.litmus"
herd_clean jam "SB.litmus"

MODEL="aarch64"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

herd_clean aarch64 "LB.litmus"
herd_clean jam "LB.litmus"

MODEL="rc11"
export MODEL
source "$SCRIPT_DIR/helpers.sh"

herd_clean rc11 "opt-invalid/lb.litmus"
herd_clean jam "opt-invalid/lb.litmus"
