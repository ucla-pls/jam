#!/bin/bash

# Convert a litmus test written in vertical format (with === as delimiter between each process) to standard format
# Usage: `./tolitmus.sh <file> > <file>.litmus`

FILE=$(<$1)
delimiter="==="
s="$FILE$delimiter"
NEWCONTENT=""
array=();

while [[ $s ]]; do
    array+=( "${s%%"$delimiter"*}" );
    s="${s#*"$delimiter"}";
done;

PNUM=${#array[@]}-2

NEWCONTENT="${array[0]}"

NEW=""
for ((j = 0; j < $PNUM-1; ++j)); do
    NEW="$NEW P$j\t|"
done
NEW="$NEW P$j\t;\n"

maxlinen=0
for ((i = 1; i < ${#array[@]}-1; ++i)); do
    CODE="${array[$i]}";
    line_n=-2
    while IFS= read -r line; do
        line_n=$(( line_n + 1 ))
    done <<< "$CODE"
    if (( $line_n > $maxlinen )); then
        maxlinen=$line_n
    fi
done

for ((k = 1; k < $maxlinen; ++k)); do
    for ((q = 1; q < ${#array[@]}-1; ++q)); do
        ICODE="${array[$q]}";
        IFS=$'\n' read -rd '' -a y <<<"$ICODE"
        if (( ${#y[@]} > $k )); then
            if (($q == ${#array[@]}-2)); then
                NEW="$NEW ${y[$k]}\t;"
            else
                NEW="$NEW ${y[$k]}\t|"
            fi
        else
            if (($q == ${#array[@]}-2)); then
                NEW="$NEW\t;"
            else
                NEW="$NEW\t|"
            fi
        fi
    done
    NEW="$NEW\n"
done

echo -e $NEWCONTENT
echo -e $NEW | column -e -n -t -s $'\t'
echo -e ${array[${#array[@]}-1]}

