# A Formalization of Java's Concurrent Access Modes 

This project contains the supplementary material for our formalization of Java's
Access Modes to appear in OOPSLA 2019 ([preprint](http://johnbender.us/assets/oopsla-2019.pdf)).

There are three parts to this README. First is a getting started guide to setup the
environment to reproduce our results. The second is information about the
contents of the `herd` subdirectory of the project and how to reproduce the
litmus test results in Section 5 (Validation) of the paper. The third is
information about the contents of the `coq` subdirectory and how they map to the
theorems of Section 6 (Metatheory).

## Getting Started

There are several methods for setting up the environment to reproduce the
results in our paper: importing a virtal machine image (`.ova` format), using
[vagrant](https://vagrantup.com) to provision a new virtual machine, automatically
installing the dependencies using provisioning scripts, or manually installing the 
necessary dependencies.

**NOTE** Building Coq from opam requires at least 4GB of RAM. The VM and
Vagrant setup are configured accordingly.

### Virtual Machine Image

A virtual machine image is available for [download](https://drive.google.com/open?id=1dDfeVIL3jMOxsbMNcxmgRFkpmc6OUQPR) (md5 `b8c62e868b2b55536932dec6a700bb0d`). 
Once the image has been downloaded you can import it using any virtualization 
software that supports the `.ova` format. To import the `jam.ova` 
file using [VirtualBox](https://www.virtualbox.org/wiki/Downloads) do the 
following:

1. Open the VirtualBox VM Manager application (GUI)
2. Click `File`
2. Click `Import Appliance`
3. Select the `jam.ova` file you downloded
4. Click `Continue`
5. Click `Import`, this will take a few minutes ...
6. Select the newly created VM in the list named `jam_default_...`
7. Click `Start`
8. When prompted login in with username `vagrant` and password `vagrant`

Once inside the VM enter the project directory with:

```bash
$ pwd
/home/vagrant
$ cd jam
```

### Vagrant

The project source includes a `Vagrantfile` configuration to provision a new VM.
First download or `git clone` the [project](https://bitbucket.org/ucla-pls/jam/src/master/),
install [Vagrant](https://vagrantup.com/downloads.html) and install
[VirtualBox](https://www.virtualbox.org/wiki/Downloads). Then change to the
project root and run:

```bash
$ vagrant up --no-provision && vagrant provision
```

After some time the VM will be set up and you can enter it using:

```bash
$ vagrant ssh
```

Once inside the VM enter the project directory;

```bash
$ pwd
/home/vagrant
$ cd jam
```

### Ubuntu Bionic

If you are already running an Ubuntu Bionic environment you can also run the
provisioning scripts manually, one as a super-user and the other as a regular 
user. 

**NOTE** This approach makes destructive updates to the system for example it
alters `$HOME/.bash_profile` to make the binaries installed by `opam` available
in the `$PATH`.

First download or `git clone` the
[project](https://bitbucket.org/ucla-pls/jam/src/master/), and then run the
following from the project root as the user for which the dependencies should be
installed:

```bash
$ sudo bash bin/vm.sh
$ bash bin/vm-user.sh
```
The first script will install dependencies through `apt-get` and also the `timeout`
utility. The second will setup Opam, Coq, and Herd.

### Dependencies 

If you prefer to manually install the depedencies in the `vm.sh` and
`vm-user.sh` scripts they are included below as configured in the VM:

- [Coq](https://github.com/coq/coq) - v8.6.1
- [Herd](https://github.com/herd/herdtools7) - v7.51
- timeout utility - [github master](https://github.com/pshved/timeout)

## Herd Model and Results

All paths in the following section are relative to the project directory. 
In the downloadable VM image and Vagrant setups that is `/home/vagrant/jam`.

### The `herd` Subdirectory

The full model from the paper can be found at `herd/jam.cat`.

The `herd` directory also contains folders for each model that we compare against:
`herd/aarch64`, `herd/rc11`, and `herd/x86`. In each of those sub-directories you
will find the `models`, `config`, and `litmus` sub directories. 

The `herd/$MODEL/models` directory contains the cat models for both the model
which we compare with and the JAM. The JAM definition contains the mapping from
the built-in relations for the comparison model to Java's access modes as defined
in each subsection of Section 5.

All of the models that we compare with come from the Herd projct in the
`herd/libdir/` sub-directory of the `herdtools7`
[respository](https://github.com/herd/herdtools7). We used the commit:
`1c04ffffb152c38718161c53b8e6b47614801b70`.

- `herd/rc11/models/rc11.cat` - [view here](https://github.com/herd/herdtools7/tree/1c04ffffb152c38718161c53b8e6b47614801b70/herd/libdir/rc11.cat)
- `herd/aarch64/models/aarch64.cat` - [view here](https://github.com/herd/herdtools7/tree/1c04ffffb152c38718161c53b8e6b47614801b70/herd/libdir/aarch64.cat)
- `herd/X86/models/x86tso.cat` - [view here](https://github.com/herd/herdtools7/tree/1c04ffffb152c38718161c53b8e6b47614801b70/herd/libdir/x86tso.cat)

The `herd/$MODEL/config` directory contains the configurations we used when running each test
in our comparison.

The `herd/$MODEL/litmus` directory contains the litmus tests we used in each comparison. 

In the case of `RC11` our own tests are at the top level of that sub-directory 
and the tests from previous works are in sub-directories. 
Here are the links where you can find the litmus tests for each test sub-directory:

- `herd/rc11/litmus/opt-invalid` - [Vafeiadis et al.](https://github.com/nchong/c11popl15/tree/master/tests)
- `herd/rc11/litmus/taming` - [Wickerson and Batty](http://multicore.doc.ic.ac.uk/opencl/)
- `herd/rc11/litmus/repairing-sc` - manually coppied from [Lahav et al.](https://plv.mpi-sws.org/scfix/paper.pdf)

In the case of `aarch64` all the tests come from [Pulte et al.](https://www.cl.cam.ac.uk/~pes20/armv8-mca/litmus-tests/)
In the case of `x86` all of the tests come from the [Herd repository](https://github.com/herd/herdtools7/tree/master/doc)

### Reproducing Litmus Test Results of Section 5

There are three sets of litmus test results as detailed in the paper; those for
C11, those for x86 and those for ARMv8. The `herd/bin` directory contains
scripts for running each the comparisons. To view the raw litmus test results 
use the following commands:

```bash
$ bash herd/bin/rc11.sh    # c11
$ bash herd/bin/X86.sh     # x86
$ bash herd/bin/aarch64.sh # armv8
```

Note that these scripts assume that the `herd7` binary is available is somewhere
in the `$PATH`. See Setup above for more.

Each script runs helper functions found in `herd/bin/helper.sh`. `herd_clean`
reduces the output from Herd to the observations only. `herd_simple` runs the a
given litmus test using the given model from within the model directory and
limits the execution time and memory.

Since running these script takes a little while it's best to save the output. For
example when running the test for C11

```bash
$ bash herd/bin/rc11.sh 2>&1 | tee herd/scratch/rc11.out
```

The the output of these scripts can be viewed as a table. For example if we
wish to view the table for the tests run for C11:

```bash
$ cat herd/scratch/rc11.out | bash herd/bin/columns.sh | column -t -s'|'
```

Figure 14, ARMv8 Litmus Test Comparison:

```bash
$ bash herd/bin/aarch64.sh 2>&1 | tee herd/scratch/aarch64.out
$ cat herd/scratch/aarch64.out | bash herd/bin/columns.sh | column -t -s'|'
```

First half of Figure 16, RC11 & x86 Litmus Test Comparison:

```bash
$ bash herd/bin/rc11.sh 2>&1 | tee herd/scratch/rc11.out
$ cat herd/scratch/rc11.out | bash herd/bin/columns.sh | column -t -s'|'
```

Second half of Figure 16, RC11 & x86 Litmus Test Comparison:

```bash
$ bash herd/bin/X86.sh 2>&1 | tee herd/scratch/X86.out
$ cat herd/scratch/X86.out | bash herd/bin/columns.sh | column -t -s'|'
```

## Coq Model and Results

All paths in the following section are relative to the project directory. 
In the downloadable VM image and Vagrant setups that is `/home/vagrant/jam`.

The `coq` directory contains the source for the mechanization of our axiomatic
model. Our mechanization is built on the work of Crary and Sulivan's
mechanization of RMC. To build the project run:

```bash
$ cd coq
$ coqc --version 
The Coq Proof Assistant, version 8.6.1 ($BUILD_DATE)
compiled on $DATE with OCaml 4.02.3
$ coq_makefile -f _CoqProject -o Makefile && make clean all
```

### Definitions and Lemmas

The semantics can be found in `coq/Dynamic.v`. Of particular importance are the
defintions of the relations from the axiomatic model. In `coq/Dynamic.v` they
begin with `to` and end with `co`. There are many, many lemmas in `History.v`
and some in `Truncate.v`.

### Mechanized Results of Section 6

- Theorem 1 is `montonicity` in `coq/Monotonicity.v`
- Lemma 1 is `vvop_irreflex` in `coq/Truncate.v`
- Theorem 2 is `drf_sc` in `coq/SC/DRF.v`
- Theorem 3 is `acq_causality` in `coq/ReleaseAcquire.v`
